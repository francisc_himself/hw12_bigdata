#!/usr/bin/python
from sklearn.datasets import load_digits
from sklearn.decomposition import PCA
from sklearn import svm
from sklearn.model_selection import learning_curve
import matplotlib.pyplot as plt
import numpy as np
import argparse
import sys

parser = argparse.ArgumentParser(description="lab12 BIG data")
parser.add_argument ("-ncomp", action='store_true', help="Number of components for SCA", required=False)
parser.add_argument ("-perc", action='store_true', help="percentage of input as training set", required=False)
# args = parser.parse_args()
if len(sys.argv[1:])==0:
    parser.print_help()
    exit(1)

##################################
gNComp = int(sys.argv[1])
gPercentage = int(sys.argv[2])
##################################

print "1) Loading digits.."
digits = load_digits()
print "     [OK] digits loaded!"
print "     [Dataset INFO:]"
print "     nb of classes: 10"
print "     dimensionality: 64 [8x8]"
print "     features: integers 0-16"
print "     Data to learn size (nb of samples):", len(digits.data)
x, y = digits.data, digits.target

print "2) PCA.."
pca = PCA(gNComp)
pca.fit(x)
x = pca.transform(x)

print "3) Training a SVM on the subset to be trained:"
svc = svm.SVC(kernel='linear')
x_train, y_train = x[:int(len(x)*gPercentage/100)], y[:int(len(y)*gPercentage/100)]
x_valid, y_valid = x[len(x_train):], y[len(y_train):]

svc.fit(x_train, y_train)
y_predicted = svc.predict(x_valid)

for an_entry in x:
    # print an_entry
    pass
ylim = None
cv = None
plt.figure()
plt.title("lab 12 Big Data")
if ylim is not None:
    plt.ylim(*ylim)
plt.xlabel("Training examples")
plt.ylabel("Score")
train_sizes, train_scores, test_scores = learning_curve(
            svc, x_train, y_train,  cv=5)
train_scores_mean = np.mean(train_scores, axis=1)
train_scores_std = np.std(train_scores, axis=1)
test_scores_mean = np.mean(test_scores, axis=1)
test_scores_std = np.std(test_scores, axis=1)
plt.grid()
plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                 train_scores_mean + train_scores_std, alpha=0.1,
                 color="r")
plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                 test_scores_mean + test_scores_std, alpha=0.1, color="g")
plt.plot(train_sizes, train_scores_mean, 'o-', color="r",
         label="Training score")
plt.plot(train_sizes, test_scores_mean, 'o-', color="g",
         label="Cross-validation score")
plt.legend(loc="best")
plt.show()



















